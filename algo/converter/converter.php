<?php
  /*
   * ENSEMBLE DU CODE TESTE AVEC PHP 5.4.45
   *
   * pour tester le code, changer la valeur des variables a la fin de ce fichier
   *
   * executer dans une console (dans le dossier contenant ce fichier):
   *   php converter.php
   */


  /*
   * hexa vers dec, algo:
   * 1 - lire l'equivalent en décimal d'un digit ($digit) dans le tableau $dictionnaire_hex_dec
   * 2 - multiplier le digit ($digit) par la puissance ($puissance) de 16 correspondante
   * 3 - faire la somme ($decimal)
   */
  function hexadecimal_en_decimal($chaineHexa) {
    $dictionnaire_hex_dec = array(
     "0"=>0, "1"=>1, "2"=>2, "3"=>3,
     "4"=>4, "5"=>5, "6"=>6, "7"=>7,
     "8"=>8, "9"=>9, "A"=>10, "B"=>11,
     "C"=>12, "D"=>13, "E"=>14, "F"=>15
    );

    /*
     * strrev inverse la chaine de caractere (utile pour le calcul avec les puissances)
     *
     * str_split crée un tableau dont les valeurs correspondent à chaque
     * caractere da chaine passée à la fonction hexadecimal_en_decimal
     */
    $chaineHexa = str_split(strrev((string)$chaineHexa));

    $decimal = 0;

    foreach($chaineHexa as $puissance => $digit) {
      //strtoupper pour s'assurer que la lecture du dictionnaire soit insensible à la casse
      $digit = strtoupper($digit);
      //on s'assure que l'on a a faire a un nombre en hexa...
      if(!array_key_exists($digit, $dictionnaire_hex_dec)){
        die("hexadecimal_en_decimal error: Veuillez fournir une chaine de caractere contenant un nombre hexadécimal -_-\"\n");
      }
      $decimal+= $dictionnaire_hex_dec[$digit] * pow(16, $puissance);
    }

    return $decimal;
  }

  /*
   * dec vers hex, algo:
   * 1 - le reste de la division entiere de $entier par 16 devient un digit
   * 2 - diviser l'entier ($entier) par 16
   * 3 - concaténer les digits ($digits) tant que $entier n'est pas à 0
   * 4 - le résultat réel de $digits se lit en sens inverse
   */
  function decimal_en_hexadecimal($entier) {
    $dictionnaire_dec_hex = array(
     0=>0, 1=>1, 2=>2, 3=>3,
     4=>4, 5=>5, 6=>6, 7=>7,
     8=>8, 9=>9, 10=>"A", 11=>"B",
     12=>"C", 13=>"D", 14=>"E", 15=>"F"
    );
    $digits = "";
    $entier = (int)$entier;

    do {
      $reste = $entier % 16;
      $entier = floor($entier / 16);
      $digits.= $dictionnaire_dec_hex[$reste]; //correspondance du $reste dans le tableau
    } while($entier > 0);

    //la lecture s'effectue en sens inverse
    return strrev($digits);
  }

  /*
   * hexa vers dec, algo:
   * 1 - lire l'equivalent en binaire des digits ($digit) dans le tableau $dictionnaire_hex_bin
   */
  function hexadecimal_en_binaire($chaineHexa) {
    $dictionnaire_hex_bin = array(
     "0"=>"0000", "1"=>"0001", "2"=>"0010", "3"=>"0011",
     "4"=>"0100", "5"=>"0101", "6"=>"0110", "7"=>"0111",
     "8"=>"1000", "9"=>"1001", "A"=>"1010", "B"=>"1011",
     "C"=>"1100", "D"=>"1101", "E"=>"1110", "F"=>"1111"
    );

    /*
     * str_split crée un tableau dont les valeurs correspondent à chaque
     * caractere da chaine passée à la fonction hexadecimal_en_binaire
     */
    $chaineHexa = str_split((string)$chaineHexa);

    $binaire = '';

    foreach($chaineHexa as $digit) {
      //strtoupper pour s'assurer que la lecture du dictionnaire soit insensible à la casse
      $digit = strtoupper($digit);

      //on s'assure que l'on a a faire a un nombre en hexa...
      if(!array_key_exists($digit, $dictionnaire_hex_bin)){
        die("hexadecimal_en_binaire error: Veuillez fournir une chaine de caractere contenant un nombre hexadécimal -_-\"\n");
      }
      $binaire.= $dictionnaire_hex_bin[$digit]." ";
    }

    return trim($binaire);
  }

  /*
   * dec vers bin, algo:
   * 1 - le reste de la division entiere de $entier par 2 devient un bit
   * 2 - diviser l'entier ($entier) par 2
   * 3 - concaténer les bits ($bits) tant que $entier n'est pas à 0
   * 4 - le résultat réel de $bits se lit en sens inverse
   *
   * TODO: formater la chaine renvoyée en quartets et compléter éventellement
   * avec des 0 pour amiéliorer la lisibilité
   */
  function decimal_en_binaire($entier) {
    $bits = "";
    $entier = (int)$entier;

    do {
      $reste = $entier % 2;
      $entier = floor($entier / 2);
      $bits.= $reste;
    } while($entier > 0);

    //la lecture s'effectue en sens inverse
    return strrev($bits);
  }

  /*
   * binaire vers dec, algo:
   * 1 - multiplier chaque bit ($bit) par la puissance ($puissance) de 2 correspondante
   * 2 - faire la somme ($decimal)
   */

  function binaire_en_decimal($chaineBin) {
    $caracteres_autorises = array("0","1");
    /*
     * strrev inverse la chaine de caractere $chaineBin (utile pour le calcul avec les puissances)
     *
     * str_split crée un tableau dont les valeurs correspondent à chaque bit de la chaine $chaineBin
     */
    $chaineBin = str_split(strrev((string)$chaineBin));

    $decimal = 0;

    foreach($chaineBin as $puissance => $bit) {
      //on s'assure que l'on a a faire a un nombre binaire...
      if(!in_array($bit, $caracteres_autorises)){
        die("binaire_en_decimal error: Veuillez fournir une chaine de caractere contenant un nombre binaire -_-\"\n");
      }
      //strtoupper pour s'assurer que la lecture du dictionnaire soit insensible à la casse
      $decimal+= $bit * pow(2, $puissance);
    }

    return $decimal;
  }

  // HEX -> DEC
  $hexEnDec = "b1";
  echo "$hexEnDec (hex) vaut ".hexadecimal_en_decimal($hexEnDec)." en décimal\n";

  // DEC -> HEX
  $decEnHex = 100000000;
  echo "$decEnHex (dec) vaut ".decimal_en_hexadecimal($decEnHex)." en hexadécimal\n";

  // HEX -> BIN
  $hexEnBin = "1A";
  echo "$hexEnBin (hex) vaut ".hexadecimal_en_binaire($hexEnBin)." en binaire\n";

  // DEC -> BIN
  $decEnBin = 3456;
  echo "$decEnBin (dec) vaut ".decimal_en_binaire($decEnBin)." en binaire\n";

  // BIN -> DEC
  $binEnDec = "11111";
  echo "$binEnDec (bin) vaut ".binaire_en_decimal($binEnDec)." en décimal\n";

?>
