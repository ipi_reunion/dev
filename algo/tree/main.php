<?php
  require_once("lib/Member.php");
  require_once("lib/Family.php");

  //family
  /*
  $louisData = array("id"=>0, "firstName"=>"Louis", "lastName"=>"Hoareau", "birthdate"=>"10/10/2015", "sex"=>"m", "father"=>1, "mother"=>2);
  $yvesData = array("id"=>1, "firstName"=>"yves", "lastName"=>"hoareau", "birthdate"=>"10/10/1990", "sex"=>"m", "father"=>3, "mother"=>4 );
  $marineData = array("id"=>2, "firstName"=>"marine", "lastName"=>"payet", "birthdate"=>"10/10/1990", "sex"=>"f", "father"=>5, "mother"=>6);
  $paulData = array("id"=>3, "firstName"=>"paul", "lastName"=>"hoareau", "birthdate"=>"10/10/1965", "sex"=>"m", "father"=>7, "mother"=>8);
  $julieData = array("id"=>4, "firstName"=>"julie", "lastName"=>"labelle", "birthdate"=>"10/10/1965", "sex"=>"f", "father"=>9, "mother"=>10);
  $lucData = array("id"=>5, "firstName"=>"luc", "lastName"=>"payet", "birthdate"=>"10/10/1940", "sex"=>"m", "father"=>11, "mother"=>12);
  $stephanieData = array("id"=>6, "firstName"=>"stephanie", "lastName"=>"desroches", "birthdate"=>"10/10/1940", "sex"=>"f", "father"=>13, "mother"=>14);
  $jeremyData = array("id"=>7, "firstName"=>"jeremy", "lastName"=>"hoareau", "birthdate"=>"10/10/1915", "sex"=>"m", "father"=>null, "mother"=>null);
  $orianneData = array("id"=>8, "firstName"=>"orianne", "lastName"=>"drula", "birthdate"=>"10/10/1915", "sex"=>"f", "father"=>null, "mother"=>null);
  $jeanData = array("id"=>9, "firstName"=>"jean", "lastName"=>"labelle", "birthdate"=>"10/10/1915", "sex"=>"m", "father"=>null, "mother"=>null);
  $laureData = array("id"=>10, "firstName"=>"laure", "lastName"=>"ange", "birthdate"=>"10/10/1915", "sex"=>"f", "father"=>null, "mother"=>null);
  $martialData = array("id"=>11, "firstName"=>"martial", "lastName"=>"payet", "birthdate"=>"10/10/1915", "sex"=>"m", "father"=>null, "mother"=>null);
  $kellyData = array("id"=>12, "firstName"=>"kelly", "lastName"=>"rowland", "birthdate"=>"10/10/1915", "sex"=>"f", "father"=>null, "mother"=>null);
  $phillipeData = array("id"=>13, "firstName"=>"phillipe", "lastName"=>"desroches", "birthdate"=>"10/10/1915", "sex"=>"m", "father"=>null, "mother"=>null);
  $aliceData = array("id"=>14, "firstName"=>"alice", "lastName"=>"saglice", "birthdate"=>"10/10/1915", "sex"=>"f", "father"=>null, "mother"=>null);

  $familyMembers = array(
    $louisData, $yvesData, $marineData, $paulData, $julieData, $lucData, $stephanieData,
    $jeremyData, $orianneData, $jeanData, $laureData, $martialData, $kellyData,
    $phillipeData, $aliceData
  );
  */

/*
  try {
    $db = new PDO('mysql:host=localhost;port=8889;dbname=genealogy;charset=utf8', 'root', 'root');
    echo "\n";
    echo 'connecté';
    echo "\n";

    $request = $db->query('SELECT * FROM members');
    $family = $request->fetchAll();

    echo "\n";
    print_r($family);

    $request->closeCursor();
  } catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
  }
*/

  $mysqli = new mysqli("127.0.0.1", "root", "root", "genealogy", 8889);
  if ($mysqli->connect_errno) {
    echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  }

  $req = $mysqli->query("SELECT * FROM members");

  echo $mysqli->host_info . "\n";
  $familyData = $req->fetch_all(MYSQLI_ASSOC);


  $family = new Family();
  foreach($familyData as $key => $data) {
    $family->addMemberFromData($data);
  }

  $family->build();

  //$louisRelations = $family->getRelationsForMember(16);

  $louisLongestBranchLength = $family->getLongestBranchLengthForId(18);
  print_r($louisLongestBranchLength);
  echo "\n";




  /*
  $louis = new Node($yves, $marine, null, 0, $louisData);

  $yves = new Node();
  $marine = new Node();
  $jeremy = new Node();
  $jean = new Node();
  $kelly = new Node();
  $Paul = new Node();


  print_r($node);*/
?>
