<?php

  /**
   *
   */
  class Node
  {
    protected $_leftChild;
    protected $_rightChild;
    protected $_id = 0;
    //protected $_data;
    protected $_parentNodes = array();

    public function __construct(Node $leftChild = null,
                                Node $rightChild = null,
                                $parentNodes = array(),
                                $id = 0,
                                $data = array() ) {
      $this->_leftChild = $leftChild;
      $this->_id = (int) $id;
      $this->_rightChild = $rightChild;
      $this->_parentNode = $parentNode;
      $this->_data = (array) $data;
    }

    public function setLeftChild(Node $leftChild) {
      $this->_leftChild = $leftChild;
    }

    public function getLeftChild() {
      return $this->_leftChild;
    }

    public function hasLeftChild() {
      return !is_null($this->_leftChild);
    }

    public function setRightChild(Node $rightChild) {
      $this->_rightChild = $rightChild;
    }

    public function getRightChild() {
      return $this->_rightChild;
    }

    public function hasRightChild() {
      return !is_null($this->_rightChild);
    }

    public function isLeaf() {
      return !$this->hasLeftChild() && !$this->hasRightChild();
    }

    public function setId($id) {
      $this->_id = (int)$id;
    }

    public function getId() {
      return $this->_id;
    }

    public function addParentNodeId($id) {
      $this->_parentNodes[] = $id;
    }

    public function getParentNodes() {
      return $this->_parentNodes;
    }

    public function setData(array $data) {
      $this->_data = $data;
    }

    public function getData() {
      return $this->_data;
    }
  }


?>
