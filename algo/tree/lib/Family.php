<?php
  require_once("Tree.php");
  require_once("Member.php");

  class Family extends Tree {

    public function addMemberFromData ($data){
      //TODO: check for members duplication?
      $node = new Member($data);
      $this->_nodes[$node->getId()] = $node;
    }

    public function build(){
      foreach($this->_nodes as $id=>$member) {
        if($member->hasMother()) {
          $mother = $this->_nodes[$member->getMotherId()];

          $member->setRightChild($mother);
          $mother->addParentNodeId($member->getId());
        }

        if($member->hasFather()) {
          $father = $this->_nodes[$member->getFatherId()];
          $member->setLeftChild($father);
          $father->addParentNodeId($member->getId());
        }
      }
    }

    public function getRelationsForMember($id) {
      //TODO: check that the tree is built
      $member = $this->getNodeFromId($id);
      return $member->getRelations();
    }

    public function getLongestBranchLengthForId($id) {
      //print_r($this->getNodeFromId($id));
      return $this->getNodeFromId($id)->getLongestBranchLength();
    }

    public function getShortestBranchLengthForId($id) {
      //print_r($this->getNodeFromId($id));
      return $this->getNodeFromId($id)->getShortestBranchLength();
    }

    public function findLongestBranch() {
      //TODO: check that the tree is built
      foreach($this->_nodes as $id => $member) {

      }
    }
  }

?>
