<?php
  require_once("Node.php");
  /*
  * leftChild: father
  * rightChild: mother
  */
  class Member extends Node {
    private $_trCounter = 0;

    protected $_firstName;
    protected $_lastName;
    protected $_birthDate;
    protected $_sex;
    protected $_fatherId;
    protected $_motherId;

    //protected $_longestBranchLength = 0;

    public function __construct($data) {
      $this->setId($data['id']);
      $this->_firstName = $data['firstname'];
      $this->_lastName = $data['lastname'];
      $this->_birthDate = $data['birthdate'];
      $this->_sex = $data['sex'];
      $this->_fatherId = $data['father'];
      $this->_motherId = $data['mother'];
    }

    public function getFirstName(){
      return $this->_firstName;
    }

    public function getLastName(){
      return $this->_lastName;
    }

    public function getBirthDate(){
      return $this->_birthDate;
    }

    public function getSex(){
      return $this->_firstSex;
    }

    public function getFather () {
      return $this->_leftChild;
    }

    public function hasFather(){
      return !is_null($this->_fatherId);
    }

    public function getFatherId() {
      return $this->_fatherId;
    }

    public function getMother() {
      return $this->_rightChild;
    }

    public function hasMother(){
      return !is_null($this->_motherId);
    }

    public function getMotherId() {
      return $this->_motherId;
    }

    public function getRelations($counter = 0) {
      $relations = "";
      $fatherRelations = "";
      $motherRelations = "";

      if($this->hasFather()) {
        $fatherRelations =  $this->getFather()->getRelations($counter+1);
      }

      if($this->hasMother()) {
        $motherRelations =  $this->getMother()->getRelations($counter+1);
      }

      $tr = $this->_getTr($counter);
      $relations.= $this->getFirstName()."\n$tr-> pere: ".$fatherRelations." \n$tr-> mere: ".$motherRelations;

      return $relations;
    }


    public function getLongestBranchLength($length = 0) {
      $length++;
      $fatherBranchLength = $length;
      $motherBranchLength = $length;

      if($this->hasFather()) {
        $fatherBranchLength = $this->getFather()->getLongestBranchLength($length);
      }

      if($this->hasMother()) {
        $motherBranchLength = $this->getMother()->getLongestBranchLength($length);
      }

      if($fatherBranchLength > $motherBranchLength) {
        $l = $fatherBranchLength;
      }
      else {
        $l = $motherBranchLength;
      }

      return $l ;
    }

    public function getShortestBranchLength($length = 0) {
      $length++;

      $fatherBranchLength = $length;
      $motherBranchLength = $length;

      echo 

      if($this->hasFather()) {
        $fatherBranchLength = $this->getFather()->getShortestBranchLength($length);
      }

      if($this->hasMother()) {
        $motherBranchLength = $this->getMother()->getShortestBranchLength($length);
      }

      if($fatherBranchLength > $motherBranchLength) {
        $l = $fatherBranchLength;
      }
      else {
        $l = $motherBranchLength;
      }

      return $l ;
    }

    private function _getTr($counter){
      $tr = "";
      for($i= 0; $i < $counter; $i++) {
        $tr.= "  ";
      }
      return $tr;
    }
  }
?>
