<?php
  require_once("Node.php");
  /**
   *
   */
  class Tree {
    protected $_root;
    protected $_nodes = array();

    public function __construct(Node $root = null) {
      $this->_root = $root;
    }

    public function addNode(Node $node) {
      //TODO check for nodes duplication?
      $this->_nodes[] = $node;
    }

    public function getNodeFromId($id){
      return $this->_nodes[(int) $id];
    }

    public function build(){
    }

  }


?>
